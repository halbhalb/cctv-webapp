#!/usr/bin/python

from cctv import app
from werkzeug.contrib.fixers import ProxyFix
import logging
from logging.handlers import RotatingFileHandler

#for production gunicorn
app.wsgi_app = ProxyFix(app.wsgi_app)

#if not app.debug:
file_handler = RotatingFileHandler('/var/log/gunicorn/gunicorn.log')
file_handler.setLevel(logging.WARNING)
app.logger.addHandler(file_handler)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8888)
