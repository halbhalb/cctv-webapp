from sqlalchemy import Table, Column, Integer, String, Float, DateTime, Enum, event
from sqlalchemy.orm import mapper
from os import urandom
from cctv import metadata, db_session
from datetime import datetime

class Spot(object):
    query = db_session.query_property()

    @classmethod
    def find_all(cls, statuses=['new', 'published']):
        return cls.query.filter(cls.status.in_(statuses)).all()
        
    def __init__(self, latitude, longitude, accuracy=None, heading=None, image=None, status='new', created=datetime.now()):
        self.hash = urandom(16).encode('hex')
        self.latitude = latitude
        self.longitude = longitude
        self.accuracy = accuracy
        self.heading = heading
        self.image = image
        self.status = status
        self.created = created

    def serialize(self):
       """Return object data in easily serializeable format"""
       return {
            'hash': self.hash,
            'latitude': self.latitude,
            'longitude': self.longitude,
            'accuracy': self.accuracy,
            'heading': self.heading,
            'image': self.image
       }

    def __repr__(self):
        return '<Spot %r>' % (self.id)

spots = Table('spots', metadata,
    Column('id', Integer, primary_key=True),
    Column('hash', String(32), index=True, nullable=False),
    Column('latitude',  Float, nullable=False),
    Column('longitude', Float, nullable=False),
    Column('accuracy',  Float),
    Column('heading', Float),
    Column('image', String(255)),
    Column('created', DateTime),
    Column('status', Enum('new', 'published', 'hidden', 'deleted'))
)

mapper(Spot, spots)