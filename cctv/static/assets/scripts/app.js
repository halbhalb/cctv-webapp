var App = {
    Views: {},
    Routers: {},
    Models: {},
    Collections: {},
    init: function() {
        App.router = new App.Routers.AppRouter();
        Backbone.history.start();    
    }
};

App.Models.Spot = Backbone.Model.extend({
    idAttribute: "hash"
});

App.Collections.Spots = Backbone.Collection.extend({
    model: App.Models.Spot,
    page: 0,
    items: null,
    fetching: false,

    url: function() {
        return "/spot/index/page/"+this.page;
    },
    fetch: function(options) {
        var self = this;
        if (self.fetching) {
            return;
        }
        self.fetching = true;
        self.page = 1;
        var success = options.success;
        options = options ? _.clone(options) : {};
        options.url = self.url();
        options.dataType = 'json';
        var nextPage = function(data, status, xhr) {
            options.url = self.url();
            self.page++;
            if (self.page==2) {
                self.reset();
                self.add(data);
                Backbone.ajax(options);
            } else {
                if (data.length) {
                    self.add(data);
                    Backbone.ajax(options);
                } else {
                    var method = options.update ? 'update' : 'reset';
                    self.fetching = false;
                    if (success) success(self.collection, self.items, options);
                }
            }
        };
        options.success = nextPage;
        return Backbone.ajax(options);
    },
});


App.Views.BaseView = Backbone.View.extend({
    template: null,

    initialize: function() {
        this.render();
    },

    render: function() {
        return this;
    }

});

App.Views.Index = App.Views.BaseView.extend({
    initialize: function() {
        this.initializeMap();
        this.markers = {};
        this.popups = {};
    },

    render: function() {
    },

    addSpot: function (spot) {
        var lat = spot.get('latitude');
        var lng = spot.get('longitude');
        var hash = spot.get('hash');
        var self = this;
        if (!this.markers[hash]) {
            var marker = L.marker([lat, lng]).addTo(self.map);
            marker.on("click", function() {
                var popup = null;
                if (!self.popups[hash]) {
                    var popupHtml = '<a href="#"><img class="popup-image" src="/spot/read/'+hash+'/image" ></a>';
                    var popupContent = $(popupHtml);
                    popupContent.click(function() { 
                        self.map.closePopup();
                    });
                    var popup = L.popup({className: 'photo-popup', minWidth: 300})
                        .setLatLng([lat+0.003, lng])
                        .setContent(popupContent[0]);
                    self.popups[hash] = popup;
                } else {
                    popup = self.popups[hash];
                }
                popup.openOn(self.map);
            })
            this.markers[hash] = marker;
        }
    },

    initializeMap: function() {
        this.map = new L.Map(this.$('#map')[0], {zoomControl:false});
        this.map.addControl(new L.Control.Zoom({position:'bottomleft'}));
        var osmUrl='http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
        var osmAttrib='Map data &copy; OpenStreetMap contributors';
        var osm = new L.TileLayer(osmUrl, {minZoom:8, attribution:osmAttrib});      
        this.map.setView(new L.LatLng(51.507, -0.128),13);
        this.map.addLayer(osm);
    }

});



App.Routers.AppRouter = Backbone.Router.extend({
	routes: {
        "": "indexAction"
	},

    indexAction: function() {
        var self=this;
        self.spots = new App.Collections.Spots();
        self.indexView = new App.Views.Index({el:$('#app'), model:self.spots});
        self.spots.bind("add",function(model,collection,opts){
            self.indexView.addSpot(model);
        });
        self.spots.fetch({
            success:function(collection, response) {
                self.indexView.render();
            },
            error:function(collection, response){
                //this happens if you reload the page
                //alert( 'Error reading data from the server.');
            }
        });
    }  
});

