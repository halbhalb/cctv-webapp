#the next step is to build a bit of a frontend with OSMap. 
#change form page so that you drag point on a map and sligly randomize the location
#then add the homepage with map and spot/index action and json rendering
from __future__ import with_statement
from cctv import app, db_session
from decorators import crossdomain
from models import Spot
from forms import SpotCreateForm, SpotUploadForm
from contextlib import closing
from flask import Flask, Response, request, session, g, redirect, url_for, abort, render_template, flash
from flaskext.uploads import UploadSet, IMAGES
from PIL import Image
from PIL.ExifTags import TAGS
import simplejson as json
import pprint
import StringIO

spots_per_page = 25

@app.teardown_request
def shutdown_session(exception=None):
    try:
        db_session.commit()
    except:
        db_session.rollback()
    db_session.remove()

@app.route('/')
def index():
    return render_template('index.html')

#hackish way to authorize blitz.io 
#probably best done using a vhost reqrite to a text file
@app.route('/mu-aa701063-10084452-de91e654-4066fe31')
def blitzio_check():
    app.logger.info('in blitzio_check')
    return "42"

@app.route('/spot/index')
@app.route('/spot/index/page/<int:page>')
def spot_index(page=1):
    spots = Spot.find_all()[(page-1)*spots_per_page:page*spots_per_page]
    serialized_spots = [spot.serialize() for spot in spots]
    return json.dumps(serialized_spots)

@app.route('/spot/create', methods=['GET', 'POST', 'OPTIONS'])
def spot_create():
    app.logger.info('in spot_create')
    error = None
    images = UploadSet('images', IMAGES)
    form = SpotCreateForm(request.form)
    if request.method == 'POST':
        spot = None
        if form.validate() and 'image' in request.files:
            filename = images.save(request.files['image'])
            spot = Spot(form.latitude.data, form.longitude.data, form.accuracy.data, form.heading.data, filename)
            db_session.add(spot)
            db_session.flush()
            return json.dumps(spot.serialize())
        else:
            return json.dumps({"error":True, "messages":form.errors}),400
    return render_template('spot_create.html', form=form)

@app.route('/spot/read/<hash>/image')
def spot_image(hash):
    spot = Spot.query.filter_by(hash=hash).first()
    images = UploadSet('images', IMAGES)
    fname = images.path(spot.image)
    img = Image.open(fname)
    ret = {}
    if hasattr( img, '_getexif' ):
        exifinfo = img._getexif()
        if exifinfo != None:
            for tag, value in exifinfo.items():
                decoded = TAGS.get(tag, tag)
                ret[decoded] = value
    if 'Orientation' in ret and ret['Orientation']!=1:
        if ret['Orientation']==8:
            img = img.transpose(Image.ROTATE_90)
        elif ret['Orientation']==3:
            img = img.transpose(Image.ROTATE_180)
        elif ret['Orientation']==6:
            img = img.transpose(Image.ROTATE_270)
        output = StringIO.StringIO()
        img.save(output, format="JPEG")
        contents = output.getvalue()
        output.close()
    else:
        contents = open(fname, 'r').read()
    return Response(contents, mimetype='image/jpeg')
