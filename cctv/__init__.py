from flask import Flask
#from flask.ext.admin import Admin
#from flask.ext.login import LoginManager
from flaskext.uploads import UploadSet, IMAGES, configure_uploads
from sqlalchemy import create_engine, MetaData
from sqlalchemy.orm import scoped_session, sessionmaker

#configuration
DEBUG = True

#db url
DATABASE = 'mysql://cctv:cctv@localhost/cctv'

#uploads
UPLOADED_IMAGES_DEST = '/var/www/cctv-webapp/cctv/static/uploads/'
UPLOADED_IMAGES_URL = '/static/uploads/'

engine = create_engine(DATABASE, convert_unicode=True)
metadata = MetaData()
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))
def init_db():
    metadata.create_all(bind=engine)


images = UploadSet('images', IMAGES)


# create our little application :)
app = Flask(__name__)
app.config.from_object(__name__)
app.secret_key = 'pl?RYW|q+FF&2? rQb*q.W2Y dTCduaKEm)(bdc)XQI*X/q/yeYY|pdzIO9m-w6m'
app.config.from_envvar('CCTV_SETTINGS', silent=True)
configure_uploads(app, (images))
#admin = Admin(app)

import cctv.views
#import cctv.admin_views

#admin.add_view(admin_views.SpotsView(db_session))
