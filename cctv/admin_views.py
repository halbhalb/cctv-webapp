from flask.ext.admin import Admin, BaseView, expose
from flask.ext.admin.contrib.sqlamodel import ModelView
from flask import Markup
from models import Spot


class SpotsView(ModelView):
    can_create = False
    column_list = ('hash', 'latitude', 'longitude', 'image', 'created', 'status')
    column_formatters = dict(image=lambda c,m,p:SpotsView.image_column_formatter(c,m,p))
    
    def __init__(self, session, **kwargs):
        # You can pass name and other parameters if you want to
        super(SpotsView, self).__init__(Spot, session, **kwargs)

    @staticmethod
    def image_column_formatter(c, m, p):
        return Markup('<img height="200" alt="'+m.image+'" src="/spot/read/'+m.hash+'/image"/>')