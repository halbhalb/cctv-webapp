from wtforms import Form, DecimalField, TextField, FileField, validators

class SpotCreateForm(Form):
    latitude = DecimalField('Latitude', [validators.Required(), validators.NumberRange(51.297, 51.703)])
    longitude = DecimalField('Longitude', [validators.Required(), validators.NumberRange(-0.602, 0.344)])
    heading = DecimalField('Heading', [])
    accuracy = DecimalField('Accuracy', [])
    image = FileField('Image', [])
    
class SpotUploadForm(Form):
    image_file = FileField('Image', [])
    
