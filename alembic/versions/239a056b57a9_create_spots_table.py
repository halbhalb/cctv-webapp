"""create spots table

Revision ID: 239a056b57a9
Revises: None
Create Date: 2012-09-13 13:58:11.168767

"""

# revision identifiers, used by Alembic.
revision = '239a056b57a9'
down_revision = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
        'spots',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('hash', sa.String(32), index=True, nullable=False),
        sa.Column('latitude',  sa.Float, nullable=False),
        sa.Column('longitude', sa.Float, nullable=False),
        sa.Column('accuracy',  sa.Float, nullable=True),
        sa.Column('heading', sa.Float, nullable=True),
        sa.Column('image', sa.String(255), nullable=True)
    )


def downgrade():
    op.drop_table('spots')
