"""add status and created timestamp to spots

Revision ID: 4437989fd3a
Revises: 239a056b57a9
Create Date: 2012-09-13 14:13:49.658818

"""

# revision identifiers, used by Alembic.
revision = '4437989fd3a'
down_revision = '239a056b57a9'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('spots', sa.Column('created', sa.DateTime))
    op.add_column('spots', sa.Column('status', sa.Enum('new', 'published', 'hidden', 'deleted')))


def downgrade():
    op.drop_column('spots', 'status')
    op.drop_column('spots', 'created')
