CREATE TABLE IF NOT EXISTS `spots` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `hash` varchar(32) CHARACTER SET utf8 NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  `accuracy` float DEFAULT NULL,
  `heading` float DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `hash` (`hash`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
